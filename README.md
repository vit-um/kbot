# Task14 Міграція Pipeline Jenkins в GitLab CI/CD

1. Реєструємось та експортуємо наш проект з GitHUB на [GitLAB](https://gitlab.com/vit-um/kbot)
2. Клонуємо репозиторій на локальну машину:  
```sh
git clone https://gitlab.com/vit-um/kbot.git
```
3. Або на сайті [GitLab](https://gitlab.com/vit-um/kbot) в репозиторії з застосунком натискаємо `Edit` -> `Web IDE`
4. Створюємо файл [.gitlab-ci.yml`](.gitlab-ci.yml) в корені проекту та починаємо описувати в ньому процес CI

## [Pros/cons оцінка міграції з Bitbucket server та Jenkins у Gitlab SaS на 8 аргументів](Pros_cons_essay.md)

## Помилково вирішив, що доробив цю задачу, але в файлі .gitlab-ci.yml лише заготовка, про яку я забув, купився на те що все зелене на поспішив здати завдання. Доробляти вже немаю часу, заяйнятий хакатоном. Можна не оцінювати. 
